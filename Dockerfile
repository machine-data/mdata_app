ARG WORKDIR="/app"

FROM python:3.11 as builder

ARG WORKDIR

ENV PYTHONFAULTHANDLER=1 \
    PYTHONHASHSEED=random \
    PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1

ENV PIP_DEFAULT_TIMEOUT=100 \
    PIP_DISABLE_PIP_VERSION_CHECK=1 \
    PIP_NO_CACHE_DIR=1 \
    POETRY_VERSION=1.5

#RUN apk add build-base libffi-dev

RUN pip install "poetry==$POETRY_VERSION" && poetry config virtualenvs.in-project true

WORKDIR ${WORKDIR}

ADD pyproject.toml poetry.lock ./

RUN poetry install --only=main --no-root

#RUN apt-get update && apt-get install -y \
#    build-essential \
#    curl \
#    software-properties-common \
#    git \
#    python3-dev \
#    libblas-dev \
#    liblapack-dev \
#    && rm -rf /var/lib/apt/lists/*
#RUN git clone --single-branch -b release https://git-ce.rwth-aachen.de/leah.tgu/mdata_app.git .
#COPY .streamlit/ .streamlit/
#COPY logic/ logic/
#COPY pages/ pages/
#COPY resources/ resources/
#COPY poetry.lock pyproject.toml streamlit_app.py ./

FROM python:3.11-slim as final
ARG WORKDIR
WORKDIR ${WORKDIR}

COPY --from=builder ${WORKDIR} .
ADD .streamlit/ .streamlit/
ADD logic/ logic/
ADD pages/ pages/
ADD resources/ resources/
ADD streamlit_app.py .streamlit ./

EXPOSE 8501

HEALTHCHECK CMD curl --fail http://localhost:8501/_stcore/health
# CMD ["./docker-entrypoint.sh"]
ENTRYPOINT ["./.venv/bin/python", "-m", "streamlit", "run", "streamlit_app.py", "--server.port=8501", "--server.address=0.0.0.0"]
