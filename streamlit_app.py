import streamlit as st

from logic.switch_page import switch_page

st.set_page_config(layout="wide")

st.title('Welcome to the Machine Data Demo Website')

got_to_demo = st.button('Go to Demo')
go_to_file_viewer = st.button('Go to File Upload')
go_to_exporter = st.button('Go to Export')
if got_to_demo:
    switch_page('demo')
elif go_to_file_viewer:
    switch_page('upload')
elif go_to_exporter:
    switch_page('export')
