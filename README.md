This is the official machine data visualization app package.

It is built atop the base mdata package and makes conversion and visualization tools contained therein interactively accessible. 