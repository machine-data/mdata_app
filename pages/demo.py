import mdata.io
import streamlit as st

from logic.page_logic import generate_page, available_widgets


@st.cache_data
def load_data():
    return mdata.io.read_machine_data_zip('resources/example.maed')


md = load_data()
selected_widget = st.sidebar.selectbox('Widget', available_widgets, format_func=lambda w: w.value)
generate_page(md, selected_widget)
