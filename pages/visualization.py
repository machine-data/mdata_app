import streamlit as st

from logic.page_logic import available_widgets, generate_page

st.set_page_config(layout="wide")
st.title('Machine Data Visualization')

selected_widget = st.sidebar.selectbox('Widget', available_widgets, format_func=lambda w: w.value)

md = st.session_state.get('uploaded_md')
if md is not None:
    generate_page(md, selected_widget)
